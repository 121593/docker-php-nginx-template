# docker-php-nginx-template

A docker template  based on [webdevops' php-nginx](https://dockerfile.readthedocs.io/en/latest/content/DockerImages/dockerfiles/php-nginx.html)
running a symfony app served by nginx

This image interacts with [its parent](https://gitlab.com/121593/docker-webapp-template) by creating vhosts for accessing other services (for now ackee, the analytics solution)

## Usage

#### Environment variables
Most [webdevops' image variables](https://dockerfile.readthedocs.io/en/latest/content/DockerImages/dockerfiles/php-nginx.html#environment-variables) can be used.

`<SERVERNAME>` string across project will be replaced by value defined in `HOSTNAME` env variable 

[Wait](https://github.com/ufoscout/docker-compose-wait/) script is included with the image, and read values defined by env var e.g :
`WAIT_HOSTS=ackee:3000,db:3306`

####dev
Application folders should be bind-mounted e.g :
```yaml
volumes:
  - 'sources/example:/app/example/production'
  - 'sources/example-beta:/app/example/beta'
```

Ports 80 & 443 should be exposed

####production

*Not ready yet*

Makes use of a multi-stage build to copy code and production dependencies and assets to the image itself

####CI

`.gitlab-ci-example.yml` includes a CI boilerplate building the image and publishing it to the associated (gitlab) registry 
